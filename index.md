# index

This content is encoded by UTF-8

## はじめに

このファイルは、ここに置いたファイルの来歴というか、説明につかいます。liloに関する何の資料かはっきりさせておくものです。

後から振り返るときに、困らないように。新しいのが上にくるスタイルがよいと思います。

## ファイル

### 2020年

- [https://lilo.linux.or.jp/resources/LILO&amp;東海道らぐオンラインミーティング 2020-08-16.md](https://lilo.linux.or.jp/resources/LILO&%e6%9d%b1%e6%b5%b7%e9%81%93%e3%82%89%e3%81%90%e3%82%aa%e3%83%b3%e3%83%a9%e3%82%a4%e3%83%b3%e3%83%9f%e3%83%bc%e3%83%86%e3%82%a3%e3%83%b3%e3%82%b0%202020-08-16.md)
    - 議事録
